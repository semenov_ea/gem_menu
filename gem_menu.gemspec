# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gem_menu/version'

Gem::Specification.new do |spec|
  spec.name          = "gem_menu"
  spec.version       = GemMenu::VERSION
  spec.authors       = ["Evgeniy Semenov"]
  spec.email         = ["evgeniy.semenov@oarts.org"]
  spec.summary       = 'Sample gem for hse menu functionality'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
