require "gem_menu/version"

module GemMenu
  # Your code goes here...
  def fetch_url(url)
    r = Net::HTTP.get_response( URI.parse( url ) )
    if r.is_a? Net::HTTPSuccess
      r.body.force_encoding("UTF-8")
    else
      nil
    end
  end
end
